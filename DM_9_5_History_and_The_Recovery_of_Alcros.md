# Important points from your history
You saw a ghost named Rosen in a tomb. In that tomb was also a man with a note saying, "Cutter's men found me, he is moving on the DC."

You saw someone who looked like the ghost of Rosen the halfling meeting in a secret room with a City official in Mornris, but didn't investigate further.

Later, you were tricked by one of Pirate Admiral Cutter's men to 'liberate' an old mausoleum, and inside found 'ghost Rosen' but held by Minotaurs.

Outside, 'Rosen'  is killed by one Cutter's pirates, Custar (who also killed Azmethias the PC) and is revealed to be a Drow who had The Siphon and was disguised.

'Rosen' had a map of the Dawn Coast with various locations crossed off

'Rosen' said he was going to introduce you to Dagir who was a champion of half-beast's rights.  After he was killed, you found Rada the half-orc who knew where Dagir was and would lead you there, heading south to Alcros.

# The Recovery of Alcros
Gudrun Redstone - Found taking care of the wounded in the main building where villagers were held by the devils. Tavern keeper with his wife.

Susan Morton - Killed in the attack on Alcros before you arrived.

After resting, then searching the village to ensure safety, you found Susan's diamond that she had promised to you, and discovered that Gudrun was a follower of Deneir, God of scribes, and at one time a high level divine spell caster.

Aina the Nixie - followed you from Kennmouth in the river. She warned that a woman was approaching the Village from the south, and asked if anyone in the village was friendly that she could talk to and ask for help reestablishing the good name of her friend St Sidvela. She will now stay in the area and help Gudrun protect the village.

The woman approaching readily accepts your hail, and comes in a hurry to ask for rest and safety. Her name is Livia Wells, and she had left Mornris with 7 followers to find Dagir and confront him for apparent offenses to Mornris. You took her to the tavern to find out she wanted to attack Dagir because he is a radical, has been harassing Mornris, and is killing for his beliefs. She pointed you to the camp she and her friends found, only for the 7 of them to be killed.

Nyquist offered Susan's diamond to Gudrun to resurrect her, and as he did, The Siphon glowed with the same symbol that Kamen's bracer did when he saved the young man from Custar's prison.

Gudrun went about the resurrection without questioning the presence of the diamond. After a tense and reverent ritual, Susan was back alive, and the villagers were overjoyed. Susan was profusely thankful, but you couldn't spend much time in revery, as Livia was insistent you must act fast to warn Mornris of the coming attack.