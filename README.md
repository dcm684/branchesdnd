# DnD Notes

## [Notes to Consider Monthly](0_Continuing_Notes.md)

## Nightly Notes
- [January 19, 2020](20200119.md) - It's a Murder and Then we Head to the Capital
- [February 21, 2020](20200221.md) - Exploration of Hijoy Brewery Basement
- [March 27, 2020](20200327.md) - Battle on the Docks and Start of Rosen's Tomb Adventure
- [April 10, 2020](20200410.md) - Completion of Rosen's Tomb Adventure
- [April 24, 2020](20200424.md) - Rothsfire and the Case of the Poisoned Apples (and the Pirates that No One Cares About)
- [May 15, 2020](20200515.md) - Island and Arrival to Refuge
- [May 29, 2020](20200529.md) - Refuge Loop (2, 3, 4)
- [June 26, 2020](20200626.md) - Saving Refuge
- [July 24, 2020](20200724.md) - Flotilla and the Two Heads
- [August 28, 2020](20200828.md) - The Cuttlefish, the Victorious Return to the Flotilla and then Mornris
- [September 25, 2020](20200925.md) - A Day at the Fair
- [October 16, 2020](20201016.md) - Lord Fathantly's Ruins
- [November 25, 2020](20201125.md) - The Mausoleum, Pt. 1
- [December 18, 2020](20201218.md) - The Mausoleum, Pt. 2
- [December 30, 2020](20201230.md) - The Mausoleum, Pt. 3
- [January 22, 2021](20210122.md) - Looking into Rosen's Note
- [February 19, 2021](20210219.md) - Pirate Hideout Continued
- [March 19, 2021](20210319.md) - Traveling South to Kenmouth
- [April 23, 2021](20210423.md) - Arrival in Kenmouth
- [July 17, 2021](20210717.md) - A Trip to the Marshes Near Kenmouth
- [October 30, 2021](20211030.md) - Enter Astranova
- [December 28, 2021](20211228.md) - Welcome Jomi and Hello Aina
- [January 21, 2022](20220121.md) - The Defeat of Fishman and the Return of Peace to Kenmouth
- [February 18, 2022](20220218.md) - Returning to Alcros and There Are Devils
- [March 18, 2022](20220318.md) - Freeing the People and a Hobgoblin Battle
- [April 16, 2022](20220416.md) - Return to Mornris / The Parties Unite
- [June 10, 2022](20220610.md) - Entering the Temple of Moriden
- [July 22, 2022](20220722.md) - The Temple of Moriden
- [September 10, 2022](20220910.md) - Arrival in Meltan
- [October 21, 2022](20221021.md) - The Defeat of Dagir
- [November 05, 2022](20221105.md) - The Gang Finds a Hit Squad
- [December 03, 2022](20221203.md) - Birch Finds a Soulmate
- [January 28, 2023](20230128.md) - Shila's Village
- [February 24, 2023](20230224.md) - Chasing Mudlings Through a Portal to Who Knows Where
- [March 24, 2023](20230324.md) - Tunnels and More Tunnels
- [April 21, 2023](20230421.md) - Underground Labyrinth and a Puzzle
- [May 19, 2023](20230519.md) - Fighting Mudling Kings and Chasing Fey
- [June 16, 2023](20230616.md) - Exiting the Caves and Meeting Will'ion, the forest dragon
- [August 18, 2023](20230818.md) - Finding an Altar and a New Mission
- [September 15, 2023](20230915.md) - Finding a Temple in the Swamp
- [November 03, 2023](20231103.md) - Looking for the Fey and Entering the Temple
- [December 15, 2023](20231215.md) - The Dungeon and Everything All at Once
- [January 12, 2024](20240112.md) - Re-entering the Dungeon
- [February 9, 2024](20240209.md) - Exploring the Dungeon
- [March 8, 2024](20240308.md) - Looking for Velanese
- [April 5, 2024](20240405.md) - Releasing Velanese
- [May 3, 2024](20240503.md) - Chasing Velanese

## Dungeon Master's Notes
- [A Murder in Boden](DM_1_A_Murder_in_Boden.md)
- [Venturing Out](DM_2_Venturing_Out.md)
- [Arrival in Mornris](DM_3_Arrival_in_Mornris.md)
- [The Defense of Mornris and Rosen's Tomb](DM_4_The_Defense_of_Mornris_and_Rosens_Tomb.md)
- [The Apple Pox](DM_5_The_Apple_Pox.md)
- [The 11<sup>th</sup> Hour](DM_6_The_11th_Hour.md)
- [Recap since Refuge](DM_7_Recap_since_Refuge.md)
- [Alcros and Kenmouth](DM_8_Alcros_and_Kenmouth.md)
- [An Investigation of Alcross](DM_9_DM_10_An_Investigation_of_Alcros.md)
- [The Recovery of Alcros](DM_9_5_History_and_After_the_Attack_in_Alcros.md)
- [The Battle of Mornris and The Revengers](DM_10_The_Battle_of_Mornris_And_The_Revengers.md)
- [Finding Velanese](DM_11_Finding_Velanese.md)
