# The Apple Pox - Notes from the DM

Feeling lost, the group makes the acquaintance of Elle Rosewood, the barmaid at the Rosewood Tavern. It seems their deeds are spreading around town and they are granted some renowned halfling hospitality. She also gossiped about a few things; 

* The dead messenger in Rosen’s tomb was working for a person named Dagir, and was sending him word that seems to indicate that Captain Cutter, who is a pirate captain of the Calmwater Sea, is going to be ‘moving on’ the Dawn Coast. Cutter’s men killed the messenger, and Custar must have hired them in parallel to the party.

* Captain Cutter has not been hostile to the people of the Dawn Coast. They are not openly pirates, and most people don’t bat an eye at the presence of pirates. 

* There was a missed apple shipment that comes from Refuge annually, and in its stead some apples came from the old mill. 

* People have been getting sick recently. The Captain of the Guard Wolfram is one of them, and an alchemist named Rothsfire has been selling a healing tonic.

* Elle agrees to look into finding a ship’s captain to take them to Refuge as a favor for helping the City

After some prying, the party finds that a Rothsfire has purchased the old mill where the apples are coming from, and agree they need to investigate. Inside they find a secret door that leads to an underground operation involving some lizardfolk and monstrous shrimp/crab creature. Rothsfire is able to escape, but they are able to trail him to his shop, and then the docks where he seems to barter passage on a ship. It’s here they decide to alert the City guard and rely on their relationship to apprehend Rothsfire. It works and the ship is blockaded until an investigation can be completed.

Meanwhile, a halfling calling himself Captain Toady approaches and offers his ship The Spirit of the Seas to sail to Refuge on Elle’s word. 

## The 11th Hour