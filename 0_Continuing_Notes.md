# Notes for Next Time
- ~~Further investigation of note (What is DC?)~~
	- DC - Dawn Coast
	- Cuter is a pirate, but not the invading pirate
- ~~Continue quest on Cuter and Rothsfire~~
	- Guards chase after him
	- We are to visit them when we return
- Return to Rosewood in 6 months for possible cut of money ([August 28, 2020](20200828.md))
- Cutter is looking for Xavelas (half elf, dark hair) ([August 28, 2020](20200828.md))
- Look into Dagir and Cutter
	- Cutter has scabbard from Drau ([December 30, 2020](20201230.md))
- Looking into Greater Resting Potion and magical armor


# Common Inventory
## Potions
- Viscous climbing potion
- Red, lesser health potion
- Potion of Dimunition
- Fire resistance potion
- 5 vials of health

## Items of value
- 6x figurines (25G)
- (? + 6) Art (25G)
- [Elven Chain](20201230.md)
- [Wand of Missile](20201219.md)
- [6x rubies (50G)](20211228.md)
- [35G](20211228.md)
- [2 small citrenes](20211228.md)
- [Jewelry from children (1500G needs fenced)](20211228.md)
- [Gold and jewels (850G)](20220121.md)
- [Assorted jewels (100G)](20220121.md)
- [Gem Encrusted bowl, 100G](20220121.md)
- [2 Platinum ring - 50G per](20220121.md)
- [5 Gems (100G)](20220218.md)

# People of Note
- Alchemists, Yung Zhoa
- Dillard from brewery
	- Was smoking cigar when we left the cellar
	- Found ash in cellar
- Enchanter seemed helpful
- Ellie - Bar maid
- Livia Wells - The Governor's Daughter

# Our characters
- Nyquist
- Kamen
- Ethlordo
- Hammur Bashman
- Villam Puer
- ~~Azmatheus~~ Died in [fight outside of Crypt](20201230.md)
- ~~Ormr~~ - Appeared in [tavern when we were looking into a map from Rosen](20210122.md) Disappeared after [arriving in Kenmouth](20210423.md)
- Astra Nova - Appeared on [return to Kenmouth](20211030.md)
- Jomi Lamar - Appeared while in [Kenmouth](20211228.md)