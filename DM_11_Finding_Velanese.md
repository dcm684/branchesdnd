- The Revengers (of the artists formerly known as The Heroes of Mornris) have been through a lot.
- A werewolf in Boden.
- Ernest goes to the FestivAle
- The First Defense of Mornris, a.k.a. Arrr Wars. (And the first introduction to the Pirates of the Calwater and by extension, The Pact of the Lich, Artifacts of the New God, and Xavelas)
- Rosen's Tomb Raiders.
- Cap'n Toady's Wild Ride and the Liberation of Refuge.
- Cutter's Deception and the death of a friend.
- Alcros, The Chapel on the Cliff, and learning that others, including Dread's Devils and Dagir the Mind Eater are interested in the artifacts
- The Invasion of the Mind Snatcher through the Governor's Daughter, and his assassination.
- The Second Defense of Mornris, and the new Teams formed
- The Hunt for Dagir
- Devil fight 2, Electric Boogaloo
- My Big Fat Elf Wedding, a.k.a. Birch's Big Day Out
- Mudmen From Another Dimension Stole My Artifact
- Lost in a Fey's Wode, a.k.a. The Dragon Doesn't Sleep Tonight
- Sticking Poles in Strange Holes
- Once More Into the Breach, or Walking Through Portals With Fey.
- The Flyin King, a.k.a. Fly up and find out.
- All The Sun God's Men

When The Revengers bravely chose to go where no mixed race group of people-like creatures had gone before by diving into a mysterious portal to chase the mudmen for the latest Artifact of the New God, they unknowingly entered a disorienting wode at the center of an old blood feud. The Dragon and The Fey. A tale as old as time. The Dragon argued that the Fey was corrupting his forest, and the Fey that he is merely stuck here by the unworthy occupants. They had to follow someone's bread crumbs to get out of this, could anyone really blame them for choosing the disembodied Fey voice?

The Fey led them to a key staff, and some holes in the ground. The corpse collecting mushrooms in the forest were none too... happy? (Can fungi be happy?) Maybe they aren't the corruption the Dragon was referring to, since they didn't want the Fey to accomplish his goals.

No matter! We're off to see the Archfey! In an ancient sunken temple. Under a swamp. Filled with undead paladins. The guardians of the temple were not talkative. But they left clues non the less. The shiniest of which was an emblem of the Sun God, that fit perfectly atop the Key Staff.

Why are they here? What task keeps their souls endlessly patrolling? Did they serve Pelor in life? Still in death? A different, more ancient, Sun God? What divine ritual were they performing when they were so rudely interrupted by an invisible gnome. Why does the Fey want that staff stuck in a hole so badly? And what is behind all those other Doors!