# June 26, 2020

# The Well
- 60' cavern with 10' ledge
- Further down the pit, you can see the rest of the "bubble"
- Multiple cutout along the ledge
    - Positions
        - 12 o'clock
        - 3 o'clock
    - Ledge is man made, but cavern is natural
    - Surprisingly circular
    - No meaningful marks, just tool marks
- See where old mine carts would have been
    - We follow them
    - Hit a fork where there's a tunnel that's boarded but no tracks
    - Walk more and see a stream crossing, 10' wide, 10' - 20' deep
    - Another offshot
    - Heading north, NNW

# Hammur 
## Orchard
- Brothers reuinite
- Grab rub and head toward the temple

## Temple
- Kneel on rug and infront of temple    
- Temple starts to rebuilt
- Hammur sees someone sitting at altar after its rebuilt
    - Same clothing s the woman in the whitespace, but different features
    - Agelesss not aged
    - Same hair
    - Knitting a scarf
    - Introduces herself as Istus
- Hammur asks who the person in the void is
    - Says it could be her, but not right now
    - She says that she and Refuge are trapped
    - She's in danger
    - Caused time to be sick in Refuge and that it needs to be fixed
    - Istus enforces fate
        - She can give us what we need and just as easily take it back
- "Time is irrelevant to Istus" she hopes that we can give her what she needs when she needs it
- She disappears but on the altar is a large spear and opalescent gem shaped like the arm on the sun dial
    - Hammur picks them up and feels like he knows what it does
        - Crushing the gem can send back the user 6 seconds (one combat move)
        - Spear
            - (+1 attack)
            - Can be recalled
            - Can pop the bubble

# The Others
## Bridge in the Tunnel
- Ethlordo throws a rock onto the middle of the bridge
    - Bridge shakes and doesn't see super sturdy
- Laying around there is tools to repair the bridge
- More cave like features in this area
- More is moving a decent pace
- Ethlordo makes it across safely
- We throw a rope across and it does a stalctite starts to drop over Ethlordo
    - It falls and is actually a cone shaped mosnter
    - Same with the stalagmite
    - Can tendril use, but cannot cause damage via the tendrils
    - Can bite
- After everyone gets their butts handed to them, Hammur appears
    - To Hammur it appears that he has been walking for 40 minutes, but it only has been 10 minutes.
    - Istus is on their side so time stuff is possible
- Disengage after only bloodying it

## Tunnel Continued
- Enter another cavern, that is well lit (source of the light)

## Lit Cavern
- Ethlordo notices similar cavern, but smaller
    - Has a 10' bubble on the other side of the cavern, hanging in the air
    - Source of the light is at the bottom of the pit, large but dim fire (frozen in time) coming from a purple worm
    - Worms are at the bottom of the pits
- Hammur gives update of trip apart from us
    - Says he stopped by the witch for a prophesy
        - Pop the small bubble
- Man leaning against the wall
    - Notices us an says that he's been waiting for us
    - Says that he is Isaac (the sheriff)
    - "You've found us" 
        - Sherrif and June? (in the bubble)
    - Asks Hammur if he knows what else is in the bubble.
    - He's risked a lot and not sure if he wants us to have it
    - We try to convince him, but he moves closer to the sphere
    - Time resets for him as well
    - He says that he's been watching us (he says that he has his ways)
    - Still refuses, he risks so much and losts so much
        - Lost all of his friends because of it
        - Afraid that we won't do something dangerous with it.
        - Wants to be sure that we won't hurt the town
    - Isaac asks for the spear
    - Hammur casts domulturgy
        - Distracts Isaac
        - Isaac pulls wand that shoots bolt at ledge under us
    - Hammur throws and hits the bubble

## Back in the bubble
- June is young again
- She is happy
- She was just trying to stop the worm. She wished that everything would stop
- She doesn't think the worm is mean. It's a mamma and looking for its babies

## Back in the Cavern
- Vision fades and we are now back in the second cavern
- At our feet is a leather bracer
- Isaac is there and happy to see her
- Isaac shoves the bracer to the side of the wall
    - We ask what it is for
    - He tells us to take it
- June freezes and out of her mouth comes Istus's voice
    - "Just pop the bubble"
- Imperceptably the fire has been moving back into the mouth after the first pop
- Hammur pops the bubble again
- Worm retracts rapidly and fire is gone

## Back in Refuge
- Back outside of town
- Bubble is now transparent
- Istus on the otherside and says that time in refuge was delayed
- Time passes inside the bubble, like fast forward
- Once a year inside has passed, the bubble dissepates, the Reuge denizens come out wanting to thank us

## Refuge Reappears
- Luca is no longer a skeleton
- Cassidy is an elder
- Isaac is now June's caretaker
- Paloma brings us a pie with a prophesy
    - She sees us becoming great heros or dead
- Bracer from Cavern
    > "The First Watch. Makes the futility of life most apparent. Taken to rest across the sea"
Bracers. Slows time for wearer. +5 to movement or +2 to ranged attacks. Action to switch
- It's only 11:05 in real time
    - Still have 1.5 days for the boat
- Isaac gave us the background
    - June liked to wander in the caverns
    - She found the bracer, but he knew it was dangerous
    - They thought that they could use it for defense, but reacted poorly, shooting out a fireball
    - Fireball caused the collapse killing others (except Cassidy)
    - Woke up the worm
    - June grabbed the bracer and then the bubble went up
- Rewarded by town
    - Precious gems worth 610
    - Split 5 ways (122 per person)
