# Enter Astranova
## Back in to Kenmouth
- Head back to town a few hours after dark
- We hear a noise
    - Realize that that was the first sound that have we heard in a while
    - Unnatural sound
    - Comes from 500 yd ahead (toward town) but is not moving closer
- Kamen stealthily approaches the sound by himself
        - Picks out a 3+ groups of humanoids in the town
            - Pieces of them are falling off
            - Swarming a group in the middle of town around the Rowan tree
            - Same type of undead from chapel
    - The tree
        - is older than the others
        - there is a path around them
        - There is a noose hanging from the tree
- A female half elf with a lyre on her back and a rapier in hand is surrounded by a group of elven guards
    - Lyre has an unnaturally loud sound. This is what we heard earlier
    - After undead keep appearing and we are getting hit, Nyquist grabs noose off of tree and flies away from action, but the zombies still go after half elf
    - After fighting 20+ creatures the team defeats them
- Nyquist and Kamen investigate the tree
    - The tree is flowering and has red berries
    - This matches up to the stories we heard about the Rowan tree
- The half elf baird gives Ethlordo a big hug then comes up to Nyquist with a rapier against his neck questioning what he is
- Her name is Astranova
    - She said that she was touring (music) and invited to this town
        - She said she was headed to Benmouth
    - She is a big time musician
    - Was traveling with her bodyguards
    - We give her our history.
    - She was aware of Cutar but not of Dagir

- There is a shallow, unmarked grave under the tree
    - It has been recently disturbed
    - It looks like someone came out of it
    - One of the skeletons that we killed came out of it
        - Had a ring with a black polished surface
            - Astranova inspects it
            - It is a ring of spell turning
            - The ring could have come from the Barrow Mounds
        - Skeleton appears to be female or elvish

- Kamen inspects the skeletons
    - There are no items of interest

- The building's are in very bad shape
    - The wood is rotten
    - Ethlordo inspects a two story building that caught his eye
        - Appears to be an official type building
        - As he entered he saw a guard that had a fallen timber from the roof fallen on him
            - Had a long sword and a bag
            - It struggled as he entered
        - Ethlordo turns around and starts to leave as the door falls on him
        - Kamen gets him out of the building and Astranova heals him

## Taking a Well Needed Rest
- It is starting to get tense in the hamlet
    - Hamlet originally had 200 or so people
    - We have two options
        - Head towards Richard, but its an hour long trip
        - Fortify a building and rest there

- Fortify a nearby cottage
    - Lots of debate on length of rest
        - Decide on a long rest
    - Astranova prays to Celune for protection
    - Place Immovable Rod in front of the door
    - 3 of us take turns on watch
        - We hear groups of skeletons wandering around town in groups

## A Lone Fisherman
- Head stealthily to the ghost ship
    - Astranova casts a cantrip on a rock and makes it illuminated and Nyquist throws it in the opposite direction
    - Stealthily head towards the coast
    - About 100 yards out there is a ghost ship with a ghost fisher casting a net
        - The ship is coming closer to the shore
        - Reminder: Richard had no opinion on the fishing ship
- There is one seaworthy boat at the dock
    - Ethlordo and Nyquist stealthily head out to the ghost ship
    - Slowly approach and hear him muttering
    - As we get closer we hear him saying "Is it here"
    - He has 3 fingers on his right hand
    - Ship looks like it has been at the bottom of the sea for a while
    - Fisherman has normal fisher clothing

- Astranova and Kamen see ripples in the water and 4 squid-like creatures appear next to the Ethlordo and Nyquist
    - The ghost and his dinghy act as if nothing is happening
    - Upon defeating these 4 another 3 appear
    - Ethlordo and Nyquist make it to the dock only taking minimal damage

## To Do
- Check on the background of the fisher
- Nyquist still has the noose
